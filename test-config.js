import 'isomorphic-fetch';

const config = {
  backendUrl: 'http://217.31.192.141/api/',
  invalidBackendUrl: 'http://localhost:0/',
  useRealBackend: false,
  domains: {
    default: 'nic.cz',
    withDNSSEC: 'nic.cz',
    withoutDNSSEC: 'nic.sk',
    unallowed: '.cz',
    nonexisting: '-.cz',
    invalid: 'foo'
  },
  testIds: {
    finished: 'f5215b789b69654f',
    nonexisting: 'abdf123456789012',
    invalid: 'foo'
  }
};

if (!config.useRealBackend) {
  /* eslint global-require: 0 */
  global.fetch = require('jest-fetch-mock');
}

export default config;
